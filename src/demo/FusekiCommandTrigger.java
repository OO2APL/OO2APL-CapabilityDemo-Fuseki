package demo;

import oo2apl.agent.Trigger;

public final class FusekiCommandTrigger implements Trigger {
	private final String command;
	
	public FusekiCommandTrigger(final String command){
		this.command = command;
	}
	
	public final String getCommand(){ 
		return this.command;
	}
}
