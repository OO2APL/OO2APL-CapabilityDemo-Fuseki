package demo;

import java.io.IOException;

import org.json.JSONObject;

import fusekiCapability.FusekiCapability;
import fusekiCapability.FusekiContext;
import oo2apl.agent.AgentBuilder;
import oo2apl.agent.AgentContextInterface;
import oo2apl.agent.Trigger;
import oo2apl.plan.builtin.SubPlanInterface; 
/**
 * The DemoAgent demonstrates some of the Fuseki control capabilities 
 * that can be used from within plans. 
 * 
 * @author Bas Testerink
 */
public final class DemoAgent extends AgentBuilder {
	
	// Prefixes that are used for queries
	private static final String prefixes = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \r\n"+
			"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \r\n"+
			"PREFIX owl: <http://www.w3.org/2002/07/owl#> \r\n"+
			"PREFIX owl2: <http://www.w3.org/2006/12/owl2#> \r\n"+
			"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \r\n"+
			"PREFIX f: <http://example.com/owl/families#> \r\n"+
			"PREFIX g: <http://example.com/owl2/families#> \r\n";
	
	// The demo query
	private static final String query = "SELECT ?subject ?predicate ?object \r\n"+
			"WHERE { \r\n"+
			"  ?subject f:hasAncestor ?object \r\n"+
			"} \r\n"+
			"LIMIT 25\r\n";
	
	public DemoAgent(final int port, final String pathToFuseki, final String pathToTemplates){ 
		// Enable Fuseki control
		super.include(new FusekiCapability(port, pathToFuseki, pathToTemplates));
		// Make the command handling scheme
		super.addExternalTriggerPlanScheme(DemoAgent::handleCommandScheme);
	}
	
	/** Add a scheme that handles the different commands. */
	private static final SubPlanInterface handleCommandScheme(final Trigger trigger, final AgentContextInterface contextInterface){
		if(trigger instanceof FusekiCommandTrigger){
			String command = ((FusekiCommandTrigger)trigger).getCommand();
			return (planInterface)->{
				// Get the context that handles the toast interface
				FusekiContext fuseki = planInterface.getContext(FusekiContext.class);
				try{
					if(command.equalsIgnoreCase("start")){
						fuseki.startServer();
					} else if(command.equalsIgnoreCase("stop")){
						fuseki.stopServer();
					} else if(command.equalsIgnoreCase("make")){
						fuseki.addDataSet("demoData", false, prefixes);
					} else if(command.equalsIgnoreCase("reasoner")){
						fuseki.enableReasoner("demoData", false);
					} else if(command.equalsIgnoreCase("load")){
						fuseki.loadOntology("demoData", "demoOntology.ttl");
					} else if(command.equalsIgnoreCase("query")){
						JSONObject answer = fuseki.query("demoData", query);
						System.out.println(answer);
					} else if(command.equalsIgnoreCase("remove")){
						fuseki.removeDataset("demoData");
					}
				} catch(InterruptedException | IOException e){
					e.printStackTrace();
				}
			};
		} else return SubPlanInterface.UNINSTANTIATED;
	}
}
