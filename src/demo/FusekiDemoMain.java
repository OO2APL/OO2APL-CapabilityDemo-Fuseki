package demo;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import oo2apl.agent.ExternalProcessToAgentInterface;
import oo2apl.defaults.messenger.DefaultMessenger;
import oo2apl.platform.AdminToPlatformInterface;
import oo2apl.platform.Platform;
/**
 * This demo goes through the steps of creating a new triple store for data 
 * and querying this data with ontological reasoning. 
 * 
 * Note; this demo requires that fuseki2 has been installed. 
 * 
 * @author bas
 *
 */
public final class FusekiDemoMain {
	// User input
	private final Scanner scanner;
	private final ExecutorService executor;

	// The agent platform
	private final AdminToPlatformInterface platform;
	private final ExternalProcessToAgentInterface agent;

	public FusekiDemoMain(){  
		// Make the console connection
		this.scanner = new Scanner(System.in);
		this.executor = Executors.newSingleThreadExecutor(); 
		
		// Get the configuration
		System.out.println("Fuseki2 directory: (e.g. /home/john/fuseki2/)");
		String pathToFuseki = this.scanner.nextLine(); 
		System.out.println("Port: (e.g. 8000)");
		int port = this.scanner.nextInt();
		
		// Make the agent
		this.platform = Platform.newPlatform(1, new DefaultMessenger());
		this.agent = platform.newAgent(new DemoAgent(port, pathToFuseki, "./fusekiConfigurationTemplates/"));

		this.executor.submit(() -> {  
			boolean exit = false; 
			int counter = 0;
			while(!exit){ 
				String input = this.scanner.nextLine(); 
				if(counter == 0) System.out.println("Type \"start\" and hit enter to start Fuseki.");
				try {
					if(input.equalsIgnoreCase("halt")){ 
						this.agent.addExternalTrigger(new FusekiCommandTrigger("stop"));
						Thread.sleep(2000); 
						exit = true;
					} else {
						boolean correctInput = false;
						if(counter == 0 && input.equalsIgnoreCase("start")){
							correctInput = true;
							counter++;
							System.out.println("Type \"make\" and hit enter to register the demo data set in Fuseki.");
						} else if(counter == 1 && input.equalsIgnoreCase("make")){
							correctInput = true;
							counter++;
							System.out.println("Type \"reasoner\" and hit enter to enable the reasoner. This should restart Fuseki.");
						} else if(counter == 2 && input.equalsIgnoreCase("reasoner")){
							correctInput = true;
							counter++;
							System.out.println("Type \"load\" and hit enter to load the demo ontology.");
						} else if(counter == 3 && input.equalsIgnoreCase("load")){
							correctInput = true;
							counter++;
							System.out.println("Type \"query\" and hit enter to query the ancestor relation.");
						} else if(counter == 4 && input.equalsIgnoreCase("query")){
							correctInput = true;
							counter++;
							System.out.println("Type \"remove\" and hit enter to remove the demo data.");
						} else if(counter == 5 && input.equalsIgnoreCase("remove")){
							correctInput = true;
							counter++;
							System.out.println("Type \"halt\" and hit enter to stop the server and halt the demo.");
						}
						if(correctInput) 
							this.agent.addExternalTrigger(new FusekiCommandTrigger(input));
					}
						
				} catch (Exception e) { 
					exit = true;
					e.printStackTrace();
				}
			}
			this.executor.shutdown();
			this.platform.haltPlatform();
		});
	} 

	public static final void main(final String[] args){
		new FusekiDemoMain();
	}
}
